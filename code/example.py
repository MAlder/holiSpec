try: from tixi import tixiwrapper  # conda module
except: import tixiwrapper

# Prepare
tixi = tixiwrapper.Tixi()
print(dir(tixi))  # Show internals for reference
tixi.openDocument("example.xml")
tixi.registerNamespacesFromDocument()  # or: tixi.registerNamespace("http://mytool.example.com", "mytool")

# Process
length, width = [tixi.getDoubleElement("/holispec/toolspecific/mytool:toolspecific/%s" % attr) for attr in ["length", "width"]]  # access elements in mytool's namespace
print("Length: %fm" % length)
print("Width:  %fm" % width)
tixi.addDoubleElementNS("/holispec/toolspecific/mytool:toolspecific", "depth", "mytool", 1., "%.1f")

# Finalize
tixi.saveDocument("example-output.xml")
tixi.close()
