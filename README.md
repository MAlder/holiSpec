# holiSpec #
`holiSpec` is a technical language (a data format with a formal definition and agreed upon semantics) for holistic computer-based collaboration and data exchange between designers in the marine sector.

This data format is under steady development and forms a result of the project [HOLISHIP](http://www.holiship.eu), which receives funding from the European Research Council (ERC) under the European Union's Horizon 2020 research and innovation programme under grant agreement no. 689074.


## How to get started ##
- In order to use [holiSpec](https://gitlab.com/dlr-sl/holiSpec) for your own software products, you must be able to process holiSpec-conformant [XML](https://en.wikipedia.org/wiki/XML) documents, either directly from your design code or using a wrapper or adaptor to your own data formats. As XML is a versatile, open markup format, there are hundereds of software libraries available to read and write XML.

    To make use of some of the advanced features of the data format, however, it is recommended to use the precompiled [TIXI](https://github.com/DLR-SC/tixi) library. It comes with support for vectors/matrices and provides interfaces for common programming languages like C/C++, Fortran, Java, Python, and even Matlab.
- Depending on the target system and programming language, download the respective TIXI build:
    - For Python development, it is recommended to use `conda` and install the prebuild TIXI package [(via)](https://anaconda.org/dlr-sc/repo/installers?type=conda&label=main):
        ```
        conda install -y --channel DLR-SC tixi
        ```
        And then in Python:
        ```python
        from tixi import tixiwrapper
        tixi = tixiwrapper.Tixi()
        tixi.openDocument("my-ship.xml")
        ```
    - For native Windows installations, download TIXI from [the binary relase page at Github](https://github.com/DLR-SC/tixi/releases). After installation, copy the wrapper script matching your language, e.g., from `./share/tixi3/python` to your Python interpreter's `./Lib/site-packages/` folder. In Python:
        ```python
        import tixiwrapper
        tixi = tixiwrapper.Tixi()
        tixi.openDocument("my-ship.xml")
        ```
    - For Matlab, modify the library path to include the `.\share\tixi3\matlab\` folder, which includes precompiled binaries for Windows
    - For Linux, use one of the above methods or follow the instructions on building your own static Tixi library.
- Once installed, write wrapper codes or interfaces to read the data from incoming holisSpec files or write data to outgoing holiSpec files. Confer [`example.py`](code/example.py) for an example integration.
- If processing the entire file is too much effort, refer to [RCE's](http://rcenvironment.de) ability to map parts of data out of and into holiSpec XML files. This step can be explained later once relevant.


## How to contribute ##
- Development is coordinated solely through this platform (public Gitlab repository), to streamline and unify the collaboration process and the tool integration landscape
- Register at [Gitlab](https://gitlab.com). Choosing to use the new website design is recommended (cf. settings)
- Install the popular collaboration tool `Git`:
    - on Windows, download e.g. [this Git client](https://github.com/git-for-windows/git/releases/tag/v2.13.3.windows.1) (If the installer hangs, kill one of the "git bash" commands in the process manager to continue); select option *Checkout Windows-style, commit Unix-style line endings*
    - on Linux, use your package manager to install Git (e.g. `sudo apt-get install git` or `sudo zypper install git`). Configure Git to use generic line-endings: `git config core.autocrlf true`
- If you know how to use signed git commits, create a private/public key pair and upload the public key signature into your Gitlab profile, and set up git accordingly to make use of the `-S` option (this si much easier on Linux than on Windows)
- Being logged in, on the Gitlab project page [holiSpec](https://gitlab.com/dlr-sl/holiSpec), click **Fork** and select your login name as the target group to fork the project into your own project space (which creates a full repository copy under your Gitlab account)
- Using the previously installed Git tool, **clone** the *forked* repository to your local computer (find the required Git URL on your cloned project page), e.g. `git clone https://gitlab.com/YOURNAME/holiSpec.git`
- Optionallay connect your checkout with the original holiSpec repository via `git remote add upstream https://gitlab.com/dlr-sl/holiSpec.git`. This allows to pull upstream changes from the original repository that you forked
- For every contribution, feature idea or bug report, open an issue on the official *holiSpec* Gitlab repository website at Gitlab under *Issues*. If you start working on their solution yourself, add your name as the *Assignee*
- Pull and merge changes from the relevant upstream branch (e.g. master, develop, milestone, ...) before working on a feature: `git pull upstream *branchname*`
- Work on your local checkout. You may also branch your local master by `git branch featurebranchname` and switch to that branch by `git checkout featurebranchname` to work on different features at different times. Learn the basics of Git to know what to do and how to merge branches
- Commit regularly and in feature-bound change sets
- Once done with a feature, sync your local repository back to your forked Gitlab project using `git push`. You may need to setup the `origin` alias to do so, and configure your name and email:
    - `git config [--global] user.email "email@mycompany.com"`  configures the email address to use for commit signing. Use `--global` to set for all your checked out projects
    - `git config [--global] user.name "My GitlabUserName"`
    - `git remote -v`  shows configured remote URLs
    - `git remote add upstream https://gitlab.com/dlr-sl/holiSpec.git`
    - `git remote set-url origin https://gitlab.com/YOURNAME/holiSpec.git`
- On your Gitlab fork create a merge request, a.k.a. pull request (to pull and integrate your contributions into the [official repository]((https://gitlab.com/dlr-sl/holiSpec.git)) by selecting *Merge Requests* on the top of your project page (or from the menu bar left), and choosing the appropriate source and target projects and branches. Don't forget to reference the issue number that you handled and solved (e.g. using text text snippet `fixes #23` will automatically close issue 23 if the pull request is merged)
- Other community members may review the changes before allowing them to be merged into the offical repository


## Community process ##
- All additions and modifications must go through the issue tracker and a pull request. All work is in the open
- To merge a pull request into the official repository, there has to be dual control workflow
- For more complex decisions, ballots may be used, workshops held, and discussions on mailing lists be necessary, prior to merging something into the official repository
- Release roadmaps and milestones should be set up for certain feature sets with a specific deadline. This could lead to official versions like `V1.0` or `HOLISHIP_Final_2020` that users may use and reference in their internal or collaboration projects. The development of these feature releases could be done on Git branches


## Tooling ##
The *holiSpec* definition is based on the XML standards, which allows a high degree of human readability while also being open to a wide range of software.

To work with *holiSpec* files computationally, it is recommended to use existing tooling of the sibling projects from the aviation sector, the [CPACS](http://cpacs.de) data format und the [RCE platform](http://rcenvironment.de). CPACS comes with a set of generic as well as specialized software libraries that may be used on Windows and Linux, from a range of programming languages:
- [TIXI](https://github.com/DLR-SC/tixi) a XML reader/writer library
- [TIGL](https://github.com/DLR-SC/tigl) a geometry operations tool that may be reused for certain data operations
- [RCE](http://rcenvironment.de) the software integration and collaboration framework that offers specially tuned integration support for XML data


## Relationship with software integration ##
The *holiSpec* data format serves as a parametric description of ship designs, including the vessel and mission definitions.
The file may contain additional information like design constraints and requirements, further environmental data, and analysis results.
It always represents a certain (immutable) state of a design during the design process and can be used as a dataset being passed around between preparation, optimization, simulation, analysis and post-processing tools inside a design workflow.

In contrast to standalone solutions, using holiSpec in RCE avoids a central database; the modified/updated versions of the dataset are passed between tools and may be merged into a final output. The workflow in RCE is data-driven and data flows freely between components.
The outcome of such a workflow can be a final optimized dataset with a resulting ship design, or just a variant computed for comparison with other design studies.

A tutorial in setting up and maintaining the RCE platfrom can be found [here](docs/rce-tutorial-configuration.md).
