# RCE introduction - the missing parts #

## About this document ##
> This document was written in the course of the EU-funded project [HOLISHIP](http://holiship.eu).
> HOLISHIP has received funding from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme under grant agreement no. 689074.

This document introduces basic concepts of RCE installation and software integration into RCE, but also adds a lot of details on server configuration and maintenance (the missing parts from the official documentation).


## Installation ##

### Prerequisites installation ###
- Install a recent 64bit Java JDK version (Java 7 or Java 8), e.g. the official build from the [Java download page](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html "the Oracle download page") at Oracle, or an OpenJDK distribution (also available for Windows)
- To download the Oracle JDK, you have to accept the license agreement and continue with the download; then follow the installation instructions (elevated rights might be required for the installation)
- On Linux systems, you might make use of your system's package manager to simplify installing Java

### RCE installation ###
> **Hint:** No admin rights are required

- Go to the [RCE website](http://rcenvironment.de)
- Click **DOWNLOAD**
- Download RCE (latest of version 8) for your target platform (Windows/Linux). Only 64bit versions are supported
- **Hint:** The user guide offered on the website is already contained in the downloaded RCE archive
- Unzip the archive (e.g. via [7-Zip](http://www.7-zip.org/ "7-Zip")) into any application folder that you have (or the future RCE user has) access to (the archive will create an `rce` subfolder when unzipped)
- Find the documentation PDF document under `rce/documentation` and have it ready


## First start ##
- Start the RCE executable `rce.exe` under `rce/`. It should pop up a splash screen and take a while to load if the system is still cold
- Alternatively start RCE via `rce.exe --profile <path_to_any_empty_folder>` to create a new RCE profile (allows separating workspaces and settings for different projects)
- When RCE asks for a workspace, choose a location where your project files and workflow data will be stored. This location can even be placed as a subfolder to the installation at `rce/workspace` without harm. **Do not** select the option to *Use this as default and do not ask again* for the workspace unless you used the `--profile` option above, as you might want to switch between workspaces to work on completely different sets of projects later on
- After workspace selection, the full screen editing environment is shown with a default selection of *views* (which form a *perspective* in the Eclipse terminology) layed out in the application screen area
- **Hint:** Once you (accidentally) selected the option to *not ask again* for the workspace selection, no matter where you unzip your current or further RCE instances, all started RCE instances will always discover the same workspace, as they look at a preconfigured location for the workspace metadata, which is by default `C:\Users\<your_name>\.rce\default\` under Windows and can later only be modified via `rce/configuration/.settings/org.eclipse.ui.ide.prefs` (increase number of last workspaces - only possible while RCE is *not* running), but more importantly in the storage area of the *last used workspace*, e.g. under `C:\Users\<your_name>\.rce\default\internal\settings.json` &rarr; `"rce.workspace.dontAskAgain":"true"` must be set to `"false"`. That pops up the selection dialog again at next RCE run
- Check out the RCE user interface and familiarize yourself with basic Eclipse concepts (minimize/maximize views, arrange views, switch perspectives, define run configurations, etc.)

**Summary**

- Storing a workspace on a directory level beneath <rce-root> is OK
- Changing workspace location after persisting this setting is hard (disable that checkbox on first run)
- The default RCE storage location is under `C:\Users\<your_name>\.rce\default\`


## Opening demo projects ##
- A good way to familiarize oneself with workflows, components and tools, are the example projects coming with RCE
- Right click into the *Project Explorer* area, select *New &rarr; Workflow Examples Project...* and select any a name for the added examples project
- A demo project with several different example workflows is created and can be examined and tested


## Create your first project ##
- On the left-hand side, in the *Project Explorer*, right-click and create a *New &rarr; Project &rarr; General &rarr; Project* (if you lost that view, open it again under *Window &rarr; Show View &rarr; Project Explorer)*
- Double-click the project in the *Project Explorer* to open it, and right-click *New Workflow...*
- Right-click the project's folder and select *New &rarr; Workflow...* and choose a name
- The workflow will be opened in the main editor area
- **Hint:** While it is possible not only to add files and folders directly into projects inside RCE, but also to link external resources form other locations on the file system, this practice is known to be fragile and not work well with every kind of RCE/Eclipse component and data access
- Build your workflow by dragging components from the right-hand side of the editor, which shows the tool palette with pre-defined and custom integrated tools
- Connect components via *Draw Connection* to define data flow and input/output dependencies. Variables of same name and type will automatically have a connection suggested when the connection editor is openend
- Find information on provided channel data types and input value handling in the documentation
- Drag components from the palette into the editor area and access the component's properties in the properties tab at the bottom of RCE (restore missing views via *Window &rarr; Show View &rarr; Properties*)


## Running workflows locally ##
- Click the green "Run/Play" button (arrow) in the icon bar below the main menu
- Optionally adapt the suggested run name
- Select a controller: This is the RCE instance that controls the flow of data through the network of RCE instances and retrieves/stores runtime data. This allows client RCE instances to detach from the RCE network, once the (remote) controller has taken over the workflow execution, unless there are local components contained in the workflow. Clients may reconnect later to the RCE network and query the state of the workflow
- Certain components (e.g. the script execution component) allow users to select further options (e.g. the executable used to run a certain script); this must be configured on the second page of the start dialog
- The workflow runs and RCE switches to a other editor view for the workflow runtime, displaying status icons per component
- The results can further be analyzed by double-clicking components in that view. Some components open additional views (e.g. **Optimizer**, **Parametric Study**)
- Individual tool outputs (`STDOUT`/`STDERR`) can be analyzed and filtered in the **Workflow Console** view
- All workflows in the network can be managed through the **Workflow List** view, including the disposal of all saved results and metadata
- Workflow timelines and data flows can be analyzed in the **Workflow Data Browser**, including file(set)s created by individual files


## Connecting to remote servers ##
- The **Network** view allows the addition of remote RCE connections
- Click the icon to add a connection or choose the option in the context menu of the shown RCE network tree
- Add host name or IP address and remote port number (configuration is explained in the manual and in the text below)
- With each successful connection added, one or more remote RCE *instances* are shown, because each started RCE instance is able to see all other connected RCE instances, transitively

> **Hint:** Connections made in the network view are not persisted over RCE restarts. Confer the section on network configuration further below


## Create a simple script-based tool ##
> **Hint:** RCE comes with a built-in Python interpreter called Jython. For simple scripts there is no need to install and configure an external Python interpreter

- Using the *Script* component from the tool palette (found under *Execution*), create a new script inside the workflow
- Create two input connections `n1` and `n2` of type `float` in the *Inputs/Outputs* tab inside the *Properties* view, after selecting the script component from the editor
- Create one output connection `n3` of type `float`
- Inputs can be read from inside the Python script via `RCE.read_input(<channel_name>)` and outputs written to via `RCE.write_output(<channel_name>, value)`. For files and directories, the value must always be an **absolute path** to the file or directory to transmit

  ```{.python}
  import sys
  sys.stderr.write("Computing...")

  n1 = RCE.read_input("n1")
  n2 = RCE.read_input("n2")
  n3 = sum([n1, n2])
  RCE.write_output("n3", n3)

  sys.stderr.write("Done.")
  ```
- Connect the script component with two matching inputs, e.g. from single-value *Input Provider* components or other components like upstream scripts
- Connect the script's output `n3` with e.g. the *Output Writer* component to write float values into files
- Run the workflow - the run wizard will ask for the location of the `python.exe` and the root folder for writing data from the writer component
- Check status icons for errors and read the messages on the `Workflow Console` to analyze potential errors
- The `RCE` object available inside the script context offers some more functions, e.g. `get_execution_count()` &rarr; `int` of invocation count from workflow engine, `RCE.get_input_names_with_datum()` &rarr; `dict_keys` or `list` of **connected** inputs (allowing conditional code branches),  `RCE.getallinputs()` &rarr; `list` of all **defined** inputs.

## Integrating any executable code (including scripts) as a published tool ##
> **Hint:** For this example to work, an external Python interpreter needs to be installed

- As an exemplary tool integration, we will run a Python script similar to the script example from the previous section. This time the Python script is, however, only an example for running any executable code which processes data from inputs to outputs (a.k.a. black box approach)
- With a text editor of your choice (or using the integrated text editor in RCE via *New &rarr; File &rarr; `test.py`* in the *Project Explorer*, then right-clicking the file *Open With &rarr; Text Editor*) create a new "tool executable" called `test.py`
- Write a Python script that consumes one number from the command line arguments, and one from a input file (from the current working directory), and computes their sum, which is output into a output file:

  ```{.python}
  # Hint: this script cannot be run with RCE's built-in Jython, because it uses the with-statement
  import sys
  sys.stderr.write("Computing...")

  n1 = float(sys.argv[1])  # read floating-point number from first command-line argument after name of the Python script
  with open("n2.dat", "r") as fd: n2 = float(fd.read().split("\n")[0])  # read floating-point number from named file
  n3 = sum([n1, n2])  # perform computation
  with open("n3.dat", "w") as fd: fd.write(str(n3))  # write output into file

  sys.stderr.write("Done.")
  ```
- To integrate a tool, one should start with the tool integration wizard, which can be found in the toolbar
- Enter tool name, icon, tool group name (for placement into the tool palette)
- Define input/output variables `n1`, `n2` and `n3` respectively and decide, if a human participant needs to approve tool outputs before forwarding them downstream in the workflow graph
- Optionally define properties, which allows (optional or mandatory) configuration of properties on each workflow run
- Define launch settings (originating tool directory, tool version, optional static work directory - otherwise use a dynamic temporary one
- Define the tool copy/removal behavior TODO from wiki
- The checkbox *Publish component after integration* will automatically create an entry in either `integration/tools/common/published.conf` or `integration/tools/cpacs/published.conf`, depending on the type of integration, thus making the tool visible to remote instances
- Provide tool execution command line(s), the wizard supports in inserting special paths into the command line:

  ```{.python}
  python "${dir:tool}\test.py" ${in:n1}
  ```
- Define a pre-execution script to convert input connections into something your tool can process:

  ```{.python}
  fd = open("n2.dat", "w")
  fd.write(str(${in:n2}))
  fd.close()
  ```
- Define a post execution script to convert tool output into a RCE output connection:

  ```{.python}
  fd = open("n3.dat", "r")
  n3 = float(fd.read().split("\n")[0])
  fd.close()
  ${out:n3} = n3
  ```
- You may save the configuration as a **JSON** file (put under selected folder into `tools/common/<toolName>`, can be re-openend via *File &rarr; Edit Tool... &rarr; Load tool configuration from file... &rarr; Save and update*) or immediately activate the integration, and can be found hereafter in the tool palette under *User integrated tools*
- *Activation* or *Updating* means putting the generated tool integration JSON file under `integration/tools/common` or `integration/tools/cpacs`, while *publishing* means adding a single line (path to tool folder) to the `published.conf` file
- **Advanced usage:** By using the *Tool Properties* tab in the tool integration wizard, users may define one or more named sets of properties (additional key-value definitions with optional default values) that RCE can write at runtime into configuration file(s) and can be defined either on a per-component basis, or at workflow start, thus enabling users to run different scenarios of workflow/tool configurations for the same workflow definition (e.g. test/production, local/distributed, single execution/multiple executions). Above tool integration example would be extended by a property set called `test`, with an additional `float` variable `n3` that the *sum* tool would optionally take into consideration

  ```{.python}
  import os, sys
  sys.stderr.write("Computing...")

  n1 = float(sys.argv[1])  # read floating-point number from first command-line argument (sys.argv[0] containsthe name of the Python script)
  with open("n2.dat", "r") as fd: n2 = float(fd.read().split("\n")[0])  # read floating-point number from named file#
  additionals = []
  if os.path.exists("Config/additional.conf"):
    with open("Config/additional.conf", "r") as fd: additionals = [float(_.split("=")[1]) for _ in fd.read().split("\n") if _.strip() != '']
  n3 = sum([n1, n2] + additionals)
  with open("n3.dat", "w") as fd: fd.write(str(n3))

  sys.stderr.write("Done.")
  ```

  Here the script checks for the existence of the properties configuration written by the tool invocation
- Another way to implement case distinction is by using inputs that are *required if connected*. In the script component a distinction can then be made by checking surrounding the access by a `try: ... except ValueError: ...´ .
- **Hint:** When modifying a tool integration configuration, you may have to remove and re-insert the tool into your workflows, and reconnect the components. Otherwise important metadata of your tool might missing in the persisted workflow information file and might no longer match the actual tool integration requirements, leading to hard to track errors


## Configuring RCE instances for use over network ##
- Each RCE instance, no matter if running with a GUI or headless (started from console and not showing a GUI), can be configured to connect to other (local or remote) RCE instances
- Ephemeral connections can be added using the GUI in the network tab, as described in Section *Connecting to remote servers*; these connection settings are lost when RCE is shut down
- To persist connections to remote RCE instances, create a `network/connections` entry in the instance's `configuration.json`, residing in its profile folder (cf. example below for exact file structure). The connection key must be unique, but has no further relevance and is displayed only on the OSGi console
- `connectAtStartup`: if set to false, no automatic connection attempt will be made by RCE, not even after the configured delay/timeout
- `instanceName`: name to show in the network RCE network instance list
- `isRelay`: if true, forwards all clients connected to the instance's server port(s) to the instance's own connections
- `isWorkflowHost`: if true, enables function as a workflow controller, while clients unrelated to the workflow may detach from the network
- `serverPorts` section: defines open ports that remote instances (e.g. users or worker nodes) may connect to
- `publishing`: default RCE components that users may use in their workflows for this RCE instance. We recommend publishing these only on workflow hosts and publishing the script component only in a secure environment or on user instances only, as it allows execution of arbitrary code. The drawback is, however, that the client instance needs a reliable network connection and must stay connected throughout workflow runs, as scripts are only allowed to run on their computers

  ```{.json}
  {
	"backgroundMonitoring": {
		"enabledIds": "basic_system_data",
		"intervalSeconds": 15
	},
	"general": {
		"comment": "Generated by RCE architecture rollout configurator (RARC) at 2017-04-05 11:40:37",
		"instanceName": "ly_hpc01_wfhost_freacs_rce_8.0.x",
		"isRelay": true,
		"isWorkflowHost": true,
		"tempDirectory": "P:\\rce80\\temp\\ly_hpc01_wfhost_freacs_rce_8.0.x"
	},
	"network": {
		"connections": {
			"remotehostname.company": {
				"autoRetryDelayMultiplier": 1.2,
				"autoRetryInitialDelay": 5,
				"autoRetryMaximumDelay": 240,
				"connectOnStartup": true,
				"host": "123.245.55.73",
				"port": 23001
			},
			"local-hpc02.company": {
				"autoRetryDelayMultiplier": 1.2,
				"autoRetryInitialDelay": 5,
				"autoRetryMaximumDelay": 240,
				"connectOnStartup": true,
				"host": "123.245.229.173",
				"port": 23031
			}
		},
		"serverPorts": {
			"auto": {
				"ip": "129.247.229.173",
				"port": 33031
			}
		}
	},
	"publishing": {
		"components": [
          "de.rcenvironment.cluster",
		  "de.rcenvironment.converger",
		  "de.rcenvironment.joiner",
		  "de.rcenvironment.optimizer",
		  "de.rcenvironment.parametricstudy",
		  "de.rcenvironment.sqlcommand",
		  "de.rcenvironment.xmlmerger"
        ]
	}
  }
  ```

  To automate the creation of `configuration.json` for a larger number of RCE instances you might also use the DLR-developed tool called RARC (RCE architecture rollout configurator, not open sourced yet).


## Common RCE components ##
RCE comes with a set of useful basic components:
- Input Provider (outputs data values to trigger workflows)
- Output Writer (writes data from its inputs to the file system)
- Joiner (combines data from several input connections)
- Switch (distributes data to several output connections)
- Database (connector that allows execution of SQL commands)
- Excel (allows interaction with and computation of data in Excel spreadsheets)
- XML loader and merger
- Script (running script programs)
- Cluster (connection with cluster queues to submit or trigger jobs)
- Converger (tune input parameters to reach a target)
- Design of Experiments
- Evaluation Memory
- Optimizer
- Parametric Study (sweep a range of input values over the workflow).


## Advanced tipps ##

### Monitoring ###
- Operators may leverage Java's management extensions (JMX) to monitor Java's JDK performance. The `rce.ini` could be extended as follows, to enable JMX access:

  ```
  -Dcom.sun.management.jmxremote.port=9876
  -Dcom.sun.management.jmxremote.authenticate=false
  -Dcom.sun.management.jmxremote.ssl=false
  ```

  and use a JMX agent of their choice to pipe operational statistics into RRD databases or other logging/monitoring systems, e.g. via [jmxtrans](https://github.com/jmxtrans/jmxtrans) into [Graphite](http://graphiteapp.org).

  Alternatively, the Java tools JConsole or [VisualVM](http://visualvm.java.net) ([Github](https://visualvm.github.io)) may be used to connect to (local or remote) RCE JVMs.

### Remote control ###
- RCE publishes several RCE-related commands through its internal OSGi console (its basic service model) and allows starting of workflows, set up of connections, and even automatic start/stop of further RCE instances through that interface
- RCE can also be run in a start-stop mode with a preconfigured workflow as if the entire workflow was an integrated tool, thus enabling RCE as a component in other workflow systems or even as part of a larger RCE workflow.

### Headless operation ###
- Windows batch file example:

  ```
  @start "comp_hpc01_relay_project_rce_8.0.x" rce.exe --headless -console -nosplash -p "X:\rce80\profiles\comp_hpc01_relay_project_rce_8.0.x"
  ```

### SSL-encrypted RCE connections ###
- tbd.

### Using RCE profiles ###
- RCE supports the notion of profiles, which allows separation of the RCE binary installation from its workspace and configuration metadata, thus allowing to run several RCE instances on the same machine, from the same binary installation
- From a command line execute `@rce.exe --profile "D:\profile_path"`
- RCE will ask for a corresponding workspace (by default in that profile folder) and create the profile including all sub-folders (e.g. `integration/internal/output/storage`), if not existent)
- Remember to adapt network settings (e.g. opened ports) in the profile's `configuration.json` if running multiple RCE instances on the same host, and adapt the `network/connections` of remote instances to point to these.

### JVM configuration ###
- To adapt the settings of the Java Virtual Machine which RCE runs on, use a text editor which supports Unix-style line endings and open `rce/rce.ini`
- Adapt the settings, e.g. for the Java memory sizes, if necessary (from all our experience, there is seldom reason to do so). You could also adapt the path for error file logging (dumped when the JVM crashes). The garbage collector settings may be tuned as well, and Java environment variables can be added via `-Dkey=value` entries
- Making mistakes like using Windows-style line endings will make it impossible to start RCE
- Beware of system updates that modify existing Java configuration. Replacing a JDK by a JRE, updating Java while RCE is running or failing to correctly update registry settings may make it impossible to restart RCE.
